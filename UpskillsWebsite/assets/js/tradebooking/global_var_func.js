//Error Constant
const DATE_RELATION_ERROR = "daterelationerror";
const GENERAL_INPUT_ERROR = "general_error"
const UNKNOWN = "unknown";


//Error code End

//Local Error Object 
var errLocalObject = {
	"error" : GENERAL_INPUT_ERROR,
	"message": "Check your input values"
};
//End Local Error Object 


/**
*	Global variables
*/

var actionDeal = 'book';
var optionDeal = 'Bond';
var checkFormat = true;
var picker = {
	singleDatePicker: true,
	calender_style: "picker_2"
};

/***
*	Global Functions
*/
function switchTab() {
	$('.tab-content').removeClass('current');		
	$("#" + window.actionDeal).addClass('current');
	if (window.actionDeal == 'book')
		$("#" +  window.actionDeal + window.optionDeal).addClass('current');
}
function errorStringCreate(errObj){
	var instance = errObj['responseText'];
	//var tokens = instance.split("[/:]"); 
	fields = ["nominalAmount", "cleanPrice","quantity", "accrualcoupon","dirtyPrice","flatSettlement", "tradeYield"];
	var field = "";
	for (var i = fields.length - 1; i >= 0; i--) {
		if (instance.indexOf(fields[i]) > -1) {
			field = fields[i];
			break;
		};
	};
	var mess = field.length == 0 ? "Unknow error" : ("You have not inputted " +field +"correctly");
	return mess;
}
function errorNoti(tab, content) {
	$('.loader-ball').removeClass('is-active');
	if (content == null)
		swal("Fail to " + tab + " deal!", "An unknowned error occured", "error");
	else
		swal("Fail to " + tab + " deal!", content, "error");
}

function activeView1AfterView2Set(view1, view2){
	if (view2.val() != ""){
		view1.prop("disabled",false);
		view1.css("background-color","white");
	} else {
		view1.val("");
		view1.prop("disabled",true);
		view1.css("background-color","#e6e6e6");
	}
}

function checkDate1LargerDate2WithNotice(Date1, Date2){
	var date = Date1.val().split('/');
	var date1 = parseInt(date[2] + date[0] + date[1]);
	var date = Date2.val().split('/');
	var date2 = parseInt(date[2] + date[0] + date[1]);
	console.log(date1 + " VS " + date2);
	if (date1 < date2) {
		errorNoti("assign a date in the", Date1.attr('placeholder') + " has to be larger than " + Date2.attr('placeholder'));
		return false;
	};
	return true;
}

function correctDateString(num,splitter){
	var str = num.toString();
	var dayAlpha = str.length - 2;
	var monthAlpha = dayAlpha - 2; 
	var day =  str.substring(dayAlpha, str.length);
	var month = str.substring(monthAlpha,dayAlpha);
	var year = str.substring(0, monthAlpha);
	return (day+splitter+month+splitter+year);
}

function successMessage(data){
	if (data[0].isOk){
		var message = "";
		var detailsArr = [];
		if (data[0].status.packageId != null) {
			message = 
			"Package Number: \n"+
			data[0].status.packageId + "\n" +
			"Contract numbers: \n";
			detailsArr = data[0].status.contracts;
			for (var i = (detailsArr.length) - 1; i >= 0; i--) {
				message += (detailsArr[i].contractId + "\n"); 
			};
		}else if (data[0].status.contractId){
			message = 
			"Contract Number: \n" + 
			data[0].status.contractId + "\n"+
			"Trade Number: \n";
			detailsArr = data[0].status.tradeInternalIds;
			for (var i = (detailsArr.length) - 1; i >= 0; i--) {
				message += (detailsArr[i] + "\n"); 
			};
		}
		swal("Book deal successful!", message, "success");

	}else
	swal(data[0].exceptions[0].code, data[0].exceptions[0].description, "error");
}
