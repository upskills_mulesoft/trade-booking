//Error Constant
const DATE_RELATION_ERROR = "daterelationerror";
const GENERAL_INPUT_ERROR = "general_error"
const UNKNOWN = "unknown";


//Error code End

//Local Error Object 
var errLocalObject = {
	"error" : GENERAL_INPUT_ERROR,
	"message": "Check your input values"
};
//End Local Error Object 

var actionDeal = 'formBook';
var optionDeal = 'Bond';
var checkFormat = true;
var picker = {
	singleDatePicker: true,
	calender_style: "picker_2"
};
$(document).ready(function(){
	$("span.tab").click(function () {
		var formId = $(this).attr("data-tab");
		$('span.tab').removeClass('current');
		$('.tab-content').removeClass('current');
		$(this).addClass('current');
		window.actionDeal = formId;
		$("#" + formId + window.optionDeal).addClass('current');
		switchTab();
	});

	$(".optionDeal").click(function () {
		window.optionDeal = $(this).attr('id').substring($(this).attr('id').indexOf('option') + 6);
		switchTab();
	});
	
	$('#bo-bond-tradeDate').daterangepicker(picker);
	$('#bo-depo-tradeDate').daterangepicker(picker);
	$('#bo-depo-stream-startperiod').daterangepicker(picker);
	$('#bo-depo-stream-matperiod').daterangepicker(picker);
	$('#bo-irs-maturity').daterangepicker(picker);
	$('#bo-irs-tradeDate').daterangepicker(picker);
	$('#bo-sf-tradeDate').daterangepicker(picker);
	$('#bo-sf-timeToMaturity').daterangepicker(picker);
	$('#bo-fx-tradeDate').daterangepicker(picker);
	$('#bo-fx-startDate').daterangepicker(picker);
	$('#bo-fx-maturityDate').daterangepicker(picker);



	$("#bo-irs-fixedleg-initCapital").on("keyup paste", function() {
    	$("#bo-irs-floatingleg-initCapital").val($(this).val());
	});

var text = $("#bo-sf-contract").val();

	$("#bo-sf-buyCurrency").on("keyup paste", function() {
    	$("#bo-sf-contract").val($(this).val()+"/");
    	text = $("#bo-sf-contract").val();
    	$("#bo-sf-sellCurrency").val("");
	});



	$("#bo-sf-sellCurrency").on("keyup paste", function() {
		$("#bo-sf-contract").val(text+$(this).val());
	});


	// $("#bo-sf-netRate").on("change", function() {
	// 	calculatesfAmountBuyCurr();
	// });



	$("#bo-sf-amountBuyCurr").on("keyup paste", function() {
		// var net = $("#bo-sf-netRate").val() != "" ? $("#bo-sf-netRate").val() : 0;
		// var amount = $("#bo-sf-amountBuyCurr").val() != "" ? $("#bo-sf-amountBuyCurr").val() : 0;
		// $("#bo-sf-amountBuyCurr2").val(net*amount);
		calculatesfAmountBuyCurr();
	});


	$("#bo-irs-tradeDate").on('hide.daterangepicker',function(ev,picker){
		isValidDate($("#bo-irs-tradeDate").val());
		if ($("#bo-irs-tradeDate").val() != ""){
			$("#bo-irs-maturity").prop('disabled',false);
		} else {
			$("#bo-irs-maturity").val("");
			$("#bo-irs-maturity").prop('disabled',true);
		}
	});


	$("#bo-sf-tradeDate").on('hide.daterangepicker',function(ev,picker){
		if ($("#bo-sf-tradeDate").val() != ""){
			$("#bo-sf-timeToMaturity").prop('disabled',false);
		} else {
			$("#bo-sf-timeToMaturity").val("");
			$("#bo-sf-timeToMaturity").prop('disabled',true);
		}

		if ($("#bo-sf-timeToMaturity").val() != "")
		{
			calculateNetRate();
		}
	});

	$("#bo-sf-tradeDate").keydown(function(e) {
    var code = e.keyCode || e.which;
    if (code === 9) {  
        $("#bo-sf-tradeDate").data('daterangepicker').hide();
    }
	});
	$("#bo-sf-timeToMaturity").keydown(function(e) {
    var code = e.keyCode || e.which;
    if (code === 9) {  
        $("#bo-sf-tradeDate").data('daterangepicker').hide();
    }
	});


	$("#bo-sf-amountBuyCurr").on("keyup paste", function() {
		var net = $("#bo-sf-netRate").val() != "" ? $("#bo-sf-netRate").val() : 0;
		var amount = $("#bo-sf-amountBuyCurr").val() != "" ? $("#bo-sf-amountBuyCurr").val() : 0;
		$("#bo-sf-amountBuyCurr2").val(net*amount);
	});

	$("#bo-sf-fxSpotRate").on("keyup paste", function() {
		calculateMargin();
		calculateNetRate();
		calculatesfAmountBuyCurr();
	});

	$("#bo-sf-fxSwapMargin").on("keyup paste", function() {
		calculateMargin();
		calculateNetRate();
		calculatesfAmountBuyCurr();
	});

	$("#bo-sf-fxSwapPoint").on("keyup paste", function() {
		calculateMargin();
		calculateNetRate();
		calculatesfAmountBuyCurr();
	});


	$("#bo-sf-tradeDate").on('hide.daterangepicker',function(ev,picker){
		if ($("#bo-sf-timeToMaturity").val() != "")
		{
			calculateNetRate();
		}
	});

	$("#bo-sf-timeToMaturity").on('hide.daterangepicker',function(ev,picker){
		calculateNetRate();
		calculatesfAmountBuyCurr();
	});


});

function checkDateRealtimeWithNotice(dateString){
	if (!isValidDate(dateString)) {
		errorNoti("input date in the ", "Check the format of your input date !");
	};
}

function calculatesfAmountBuyCurr(){
	var net = $("#bo-sf-netRate").val() != "" ? $("#bo-sf-netRate").val() : 0;
	var amount = $("#bo-sf-amountBuyCurr").val() != "" ? $("#bo-sf-amountBuyCurr").val() : 0;
	$("#bo-sf-amountBuyCurr2").val(net*amount);
}

function toggleItem(name){
	$("#"+name).fadeIn(1000)
	if (name == "forwardDeal"){
		$("#spotDeal").fadeOut(1000);
		$("#spotDealVal").fadeOut(1000);
		$("#bo-sf-fxSpotRate").val(0);
		$("#bo-sf-margin").val(0);
	}
	else if (name == "spotDeal"){
		$("#forwardDeal").fadeOut(1000);
		$("#bo-sf-fxSwapPoint").val(0);
		$("#bo-sf-fxSwapMargin").val(0);
		$("#bo-sf-margin").val(0);
	}
}

function calculateMargin(){
	var spot = $("#bo-sf-fxSpotRate").val() != "" ? $("#bo-sf-fxSpotRate").val() : 0;
	var swap = $("#bo-sf-fxSwapMargin").val() != "" ? $("#bo-sf-fxSwapMargin").val() : 0;
	$("#bo-sf-margin").val(parseFloat(spot)+parseFloat(swap));
}

function calculateNetRate()
{
	var x = new Date($("#bo-sf-timeToMaturity").val());
	var y = new Date($("#bo-sf-tradeDate").val());
	var newdate = new Date(y);
	newdate.setDate(newdate.getDate()+2);
	var nd = new Date(newdate);

	if (x.getTime() > nd.getTime()){
        toggleItem('forwardDeal');

    	var spot = $("#bo-sf-fxSpot").val() != "" ? $("#bo-sf-fxSpot").val() : 0;
		var swap = $("#bo-sf-fxSwapPoint").val() != "" ? $("#bo-sf-fxSwapPoint").val() : 0;
		calculateMargin();
		var margin = $("#bo-sf-margin").val() != "" ? $("#bo-sf-margin").val() : 0;

		var temp = parseFloat(spot) + (parseFloat(swap)/100);
		temp += (parseFloat(margin)/100);
		$("#bo-sf-netRate").val(temp);

		var contract = document.createElement('span');
        document.getElementById('bo-sf-name').innerHTML = "";
        contract.innerHTML = "Forward Deal";
        document.getElementById('bo-sf-name').appendChild(contract);

	}

	else if (x.getTime() === nd.getTime()){
        toggleItem('spotDeal');
        toggleItem('spotDealVal');

		var spot = $("#bo-sf-fxSpot").val() != "" ? $("#bo-sf-fxSpot").val() : 0;
		var swap = $("#bo-sf-fxSwapPoint").val() != "" ? $("#bo-sf-fxSwapPoint").val() : 0;
		calculateMargin();
		var margin = $("#bo-sf-margin").val() != "" ? $("#bo-sf-margin").val() : 0;

		var temp = parseFloat(spot) + (parseFloat(margin)/100);
		$("#bo-sf-netRate").val(temp);

		var contract = document.createElement('span');
        document.getElementById('bo-sf-name').innerHTML = "";
        contract.innerHTML = "Spot Deal";
        document.getElementById('bo-sf-name').appendChild(contract);

	}
	else{
		document.getElementById('bo-sf-name').innerHTML = "";
		swal(
			'Oopps!!',
			'Maturity date can not be less than trade date!',
			'error'
			);
		$("#bo-sf-timeToMaturity").val("");
	} 
		
}

function IRSfixedRateChange()
{
	var fix = document.getElementById("bo-irs-fixedRate").value;
	var float = document.getElementById("bo-irs-floatingRate");
	if(fix == 1){
		float.value = 2;
	}
	else{
		float.value = 1;
	}
}

function IRSfloatingRateChange()
{
	var fix = document.getElementById("bo-irs-fixedRate");
	var float = document.getElementById("bo-irs-floatingRate").value;
	if(float == 1){
		fix.value = 2;
	}
	else{
		fix.value = 1;
	}
}					




function switchTab() {
	var formId = window.actionDeal + window.optionDeal;	
	$('.tab-content').removeClass('current');
	$("#" + formId).addClass('current');
}
function errorStringCreate(errObj){
	var instance = errObj['responseText'];
	// var tokens = instance.split("[/:]"); 
	fields = ["nominalAmount", "cleanPrice","quantity", "accrualcoupon","dirtyPrice","flatSettlement","tradeYield"];
	var field = "";
	for (var i = fields.length - 1; i >= 0; i--) {
		if (instance.indexOf(fields[i]) > -1) {
			field = fields[i];
			break;
		};
	};
	var mess = field.length == 0 ? "Unknow error" : ("You have not inputted " +field +"correctly");
	return mess;
}
function errorNoti(tab, content) {
	$('.loader-ball').removeClass('is-active');
	if (content == null)
		swal("Fail to " + tab + " deal!", "An unknowned error occured", "error");
	else
		swal("Fail to " + tab + " deal!", content, "error");
}
function exportDeal() {
	$('.loader-ball').addClass('is-active');
	var baseUri = 'http://murex-trade-repository-api-2.cloudhub.io/api/';
	var path = (window.optionDeal == 'Bond') ? 'trade/IRD/bond' : (window.optionDeal == 'IRS') ? 'trade/IRD/interestRateSwap' : (window.optionDeal == 'Spot') ? 'trade/CURR/FXD/spotForward' : (window.optionDeal == 'Depo') ? 'trade/IRD/loanDeposit' : 'trade/CURR/FXD/forexSwap';
	var type = $('#ex-type-' + window.optionDeal).val();	
	console.log(type);
	var link = baseUri + path + '?ids=' + $('#ex-id-' + window.optionDeal).val() + '&objectType=' + type.substring(type, type.indexOf(' '));					
	$.ajax({
		url: link,
		type: "GET",
		//async: false,
		dataType: "json",
		success: function (data) {	
			console.log(data);
			if (path.includes('bond')){
				exportBond(data[0]);
			} else
			if (path.includes('interestRateSwap')){
				exportIRS(data[0]);
			} else
			if (path.includes('spotForward')){
				exportSpot(data[0]);
			} else
			if (path.includes('loanDeposit')){
				exportDepo(data[0]);
			} else
				exportFXSwap(data[0]);
			$('.loader-ball').removeClass('is-active');
		},
		error: function(){			
			errorNoti("export");
		}
	});
}
function bookDeal() {
	$('.loader-ball').addClass('is-active');
	var baseUri = 'http://murex-trade-repository-api-2.cloudhub.io/api/';
	var path = (window.optionDeal == 'Bond') ? 'trade/IRD/bond' : (window.optionDeal == 'IRS') ? 'trade/IRD/interestRateSwap' : (window.optionDeal == 'Spot') ? 'trade/CURR/FXD/spotForward' : (window.optionDeal == 'Depo') ? 'trade/IRD/loanDeposit' : 'trade/CURR/FXD/forexSwap';
	var body = null;
	
	try {
		body = (window.optionDeal == 'Bond') ? bookBond() : (window.optionDeal == 'IRS') ? bookIRS() : (window.optionDeal == 'Spot') ? bookSpot() : (window.optionDeal == 'Depo') ? bookDepo() : bookFXSwap();
	}catch (exception){
		console.log(exception);
		errorNoti ("book",exception.message);
		return;
	}		
	console.log(body);
	var link = baseUri + path;
	if (checkFormat){
			$.ajax({
				url: link,
				type: "POST",
				//async: false,
				dataType: "json",
				data: JSON.stringify(body),
				contentType:"application/json; charset=utf-8",
				success: function (data) {
					console.log(data);
					if (data[0].isOk)
						swal("Book deal successful!", "Contract Number: " + data[0].status.contractId + "\nTrade Number: " + data[0].status.tradeInternalId, "success");
					else
						swal(data[0].exceptions[0].code, data[0].exceptions[0].description, "error");
					$('.loader-ball').removeClass('is-active');
				},
				error: function(errObj){		
					errorNoti("book", errorStringCreate(errObj));	  
				}
			});
		}																 
		else errorNoti("book", "There exists invalid fields, please fix it and try again!");											  
}
function exportBond(data) {
	$('#ex-bond-counterParty').val(data.jsonAnswer.counterParty);
	$('#ex-bond-portfolio').val(data.jsonAnswer.portfolio);
	$('#ex-bond-tradeDate').val(data.jsonAnswer.tradeHeader.tradeDate);
	$('#ex-bond-action').val(data.jsonAnswer.tradeBody.bond.action);
	$('#ex-bond-security').val(data.jsonAnswer.tradeBody.bond.securityDisplayLabel);
	$('#ex-bond-yield').val(data.jsonAnswer.tradeBody.bond.tradeYield);
	$('#ex-bond-quantity').val(data.jsonAnswer.tradeBody.bond.quantity);
	$('#ex-bond-nominal').val(data.jsonAnswer.tradeBody.bond.nominalAmount);
	$('#ex-bond-cleanPrice').val(data.jsonAnswer.tradeBody.bond.cleanPrice);
}
function exportIRS(data) {
	$('#ex-irs-counterParty').val(data.jsonAnswer.counterParty);
	$('#ex-irs-portfolio').val(data.jsonAnswer.portfolio);
	$('#ex-irs-tradeDate').val(data.jsonAnswer.tradeHeader.tradeDate);
	$('#ex-irs-template').val(data.jsonAnswer.tradeBody.interestRateSwap.templateLabel);
	
	$('#ex-irs-fixedRate').val(data.jsonAnswer.tradeBody.interestRateSwap.fixedRatePayerReference);					
	$('#ex-irs-floatingRate').val(data.jsonAnswer.tradeBody.interestRateSwap.floatingRatePayerReference);					
	$('#ex-irs-stubPeriod').val(data.jsonAnswer.tradeBody.interestRateSwap.stubPeriodPosition);										
	$('#ex-irs-fixedleg-initCapital').val(data.jsonAnswer.tradeBody.interestRateSwap.fixedStream.initialCapitalAmount);
	$('#ex-irs-fixedleg-initCapitalCurr').val(data.jsonAnswer.tradeBody.interestRateSwap.fixedStream.initialCapitalCurrency);					
	$('#ex-irs-fixedleg-rateConvention').val(data.jsonAnswer.tradeBody.interestRateSwap.fixedStream.rateConvention);
	$('#ex-irs-fixedleg-paymentCalendar').val(data.jsonAnswer.tradeBody.interestRateSwap.fixedStream.paymentCalendar);
	
	$('#ex-irs-fixedleg-frequency').val(data.jsonAnswer.tradeBody.interestRateSwap.fixedStream.frequency.periodUnit);
	$('#ex-irs-fixedleg-multiplier').val(data.jsonAnswer.tradeBody.interestRateSwap.fixedStream.frequency.periodMultiplier);
	
	$('#ex-irs-maturity').val(data.jsonAnswer.tradeBody.interestRateSwap.fixedStream.maturity);					
	$('#ex-irs-fixedleg-rate').val(data.jsonAnswer.tradeBody.interestRateSwap.fixedStream.fixedRate);
	$('#ex-irs-floatingleg-index').val(data.jsonAnswer.tradeBody.interestRateSwap.floatingStream.floatingRateStreamTemplate);
	$('#ex-irs-floatingleg-fixingCalendar').val(data.jsonAnswer.tradeBody.interestRateSwap.floatingStream.fixingCalendar);
	$('#ex-irs-floatingleg-initCapital').val(data.jsonAnswer.tradeBody.interestRateSwap.floatingStream.initialCapitalAmount);
	$('#ex-irs-floatingleg-initCapitalcurr').val(data.jsonAnswer.tradeBody.interestRateSwap.floatingStream.initialCapitalCurrency);
	$('#ex-irs-floatingleg-rateConvention').val(data.jsonAnswer.tradeBody.interestRateSwap.floatingStream.rateConvention);
	$('#ex-irs-floatingleg-paymentCalendar').val(data.jsonAnswer.tradeBody.interestRateSwap.floatingStream.paymentCalendar);
	$('#ex-irs-floatingleg-frequency').val(data.jsonAnswer.tradeBody.interestRateSwap.floatingStream.frequency.periodUnit);
	$('#ex-irs-floatingleg-multipier').val(data.jsonAnswer.tradeBody.interestRateSwap.floatingStream.frequency.periodMultiplier);				
}
function exportDepo(data) {
	$('#ex-depo-counterParty').val(data.jsonAnswer.counterParty);
	$('#ex-depo-portfolio').val(data.jsonAnswer.portfolio);
	$('#ex-depo-tradeDate').val(data.jsonAnswer.tradeHeader.tradeDate);
	$('#ex-depo-payout').val(data.jsonAnswer.tradeBody.loanDeposit.payOut);
	$('#ex-depo-stubPeriod').val(data.jsonAnswer.tradeBody.loanDeposit.stubPeriodPosition);
	$('#ex-depo-nominal').val(data.jsonAnswer.tradeBody.loanDeposit.Stream.initialCapitalAmount);
	$('#ex-depo-currency').val(data.jsonAnswer.tradeBody.loanDeposit.Stream.initialCapitalCurrency);
	$('#ex-depo-stream-rateConvention').val(data.jsonAnswer.tradeBody.loanDeposit.Stream.rateConvention);
	$('#ex-depo-stream-rate').val(data.jsonAnswer.tradeBody.loanDeposit.Stream.rate);
	$('#ex-depo-stream-startperiod').val(data.jsonAnswer.tradeBody.loanDeposit.Stream.effectiveDate);
	$('#ex-depo-stream-matperiod').val(data.jsonAnswer.tradeBody.loanDeposit.Stream.maturity);
	$('#ex-depo-stream-frequency').val(data.jsonAnswer.tradeBody.loanDeposit.Stream.frequency.periodUnit);
	$('#ex-depo-stream-multiplier').val(data.jsonAnswer.tradeBody.loanDeposit.Stream.frequency.periodMultiplier);
}
function exportSpot(data) {
	$('#ex-sf-counterParty').val(data.jsonAnswer.counterParty);
	$('#ex-sf-portfolio').val(data.jsonAnswer.portfolio);
	$('#ex-sf-tradeDate').val(data.jsonAnswer.tradeHeader.tradeDate);
	$('#ex-sf-timeToMaturity').val(data.jsonAnswer.tradeBody.fxSpotForward.dateLabel);
	$('#ex-sf-buyCurrency').val(data.jsonAnswer.tradeBody.fxSpotForward.buyCurrency);
	$('#ex-sf-sellCurrency').val(data.jsonAnswer.tradeBody.fxSpotForward.sellCurrency);
	$('#ex-sf-amountBuyCurr').val(data.jsonAnswer.tradeBody.fxSpotForward.amountBuyCurrency);
	$('#ex-sf-netRate').val(data.jsonAnswer.tradeBody.fxSpotForward.netRate);
	$('#ex-sf-margin').val(data.jsonAnswer.tradeBody.fxSpotForward.margin);
	$('#ex-sf-fxSpotRate').val(data.jsonAnswer.tradeBody.fxSpotForward.fxSpotRate);
	$('#ex-sf-fxSpotMargin').val(data.jsonAnswer.tradeBody.fxSpotForward.fxSpotMargin);
	$('#ex-sf-nonDeliverable').val(data.jsonAnswer.tradeBody.fxSpotForward.nonDeliverable);
}
function exportFXSwap(data) {
	$('#ex-fx-counterParty').val(data.jsonAnswer.counterParty);
	$('#ex-fx-portfolio').val(data.jsonAnswer.portfolio);
	$('#ex-fx-tradeDate').val(data.jsonAnswer.tradeHeader.tradeDate);
	$('#ex-fx-startDate').val(data.jsonAnswer.tradeBody.forexSwapLeg1.date);
	$('#ex-fx-buyCurrency1').val(data.jsonAnswer.tradeBody.forexSwapLeg1.buyCurrency);
	$('#ex-fx-sellCurrency1').val(data.jsonAnswer.tradeBody.forexSwapLeg1.sellCurrency);
	$('#ex-fx-amountBuyCurr1').val(data.jsonAnswer.tradeBody.forexSwapLeg1.amountBuyCurrency);
	$('#ex-fx-netRate1').val(data.jsonAnswer.tradeBody.forexSwapLeg1.netRate);
	$('#ex-fx-margin1').val(data.jsonAnswer.tradeBody.forexSwapLeg1.margin);
	$('#ex-fx-fowardDelivery').val(data.jsonAnswer.tradeBody.forexSwapLeg1.forwardDelivery);
	$('#ex-fx-maturityDate').val(data.jsonAnswer.tradeBody.forexSwapLeg2.date);
	$('#ex-fx-buyCurrency2').val(data.jsonAnswer.tradeBody.forexSwapLeg2.buyCurrency);
	$('#ex-fx-sellCurrency2').val(data.jsonAnswer.tradeBody.forexSwapLeg2.sellCurrency);
	$('#ex-fx-amountBuyCurr2').val(data.jsonAnswer.tradeBody.forexSwapLeg2.amountBuyCurrency);
	$('#ex-fx-netRate2').val(data.jsonAnswer.tradeBody.forexSwapLeg2.netRate);
	$('#ex-fx-margin2').val(data.jsonAnswer.tradeBody.forexSwapLeg2.margin);
	$('#ex-fx-nonDeliverable').val(data.jsonAnswer.tradeBody.forexSwapLeg2.nonDeliverable);	
}
// function isValidDate(dateString) {
//     if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
//         return false;
//     var parts = dateString.split("/");
//     var day = parseInt(parts[1], 10);
//     var month = parseInt(parts[0], 10);
//     var year = parseInt(parts[2], 10);
//     if(year < 1000 || year > 3000 || month == 0 || month > 12)
//         return false;
//     var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
//     if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
//         monthLength[1] = 29;
//     return day > 0 && day <= monthLength[month - 1];
// };
function bookBond() {
	// checkFormat = true;
	var Counterparty = $('#bo-bond-counterParty').val();
	var Portfolio = $('#bo-bond-portfolio').val();
	var date = $('#bo-bond-tradeDate').val().split('/');
	var Tradedate = parseInt(date[2] + date[0] + date[1]);
	var buysell = $('#bo-bond-action').val();
	var Security = $('#bo-bond-security').val();			  
	var Yield = parseFloat($('#bo-bond-yield').val());
	var Quantity = parseFloat($('#bo-bond-quantity').val());
	var Nominal = parseFloat($('#bo-bond-nominal').val());
	var Cleanprice = parseFloat($('#bo-bond-cleanPrice').val());
	var AccrualCoupon = parseFloat($('#bo-bond-accrualcoup').val());
	var	FlatSettlement = parseFloat($('#bo-bond-flatsettle').val());
	var	DirtyPrice = parseFloat($('#bo-bond-dirtyprice').val());						   
	var data = [
		{
			"userName": "USER01",
			"party": "TMB Bangkok",
			"counterParty": Counterparty,
			"portfolio": Portfolio,
			"tradeHeader": {
				"tradeDate": Tradedate,
				"tradeDestination": "external"
			},
			"tradeBody": {
				"bond": {
					"action": buysell,
					"securityMarket": "USD_GOVT",
					"securityDisplayLabel": Security,
					"currency": "USD",
					"tradeYield": Yield,
					"quantity": Quantity,
					"nominalAmount": Nominal,
					"cleanPrice": Cleanprice
				}
			}
		}
	];
	return data;
}
function bookIRS() {
	checkFormat = true;	
	var Counterparty = $('#bo-irs-counterParty').val();
	var Portfolio = $('#bo-irs-portfolio').val();
	var date = $('#bo-irs-tradeDate').val().split('/');
	var Tradedate = parseInt(date[2] + date[0] + date[1]);
	var Template = $('#bo-irs-template').val();
	var Fixedrate = $('#bo-irs-fixedRate').val();
	var stubperiod = $('#bo-irs-stubPeriod').val();
	var Floatingrate = $('#bo-irs-floatingRate').val();
	var FixedlegInitCapital = parseFloat($('#bo-irs-fixedleg-initCapital').val());
	var FixedlegInitCapitalCurr = $('#bo-irs-fixedleg-initCapitalCurr').val();
	var FixedlegRateconvention = $('#bo-irs-fixedleg-rateConvention').val();
	var FixedlegPaymentcalendar = $('#bo-irs-fixedleg-paymentCalendar').val();
	var FixedlegFrequencyUnit = $('#bo-irs-fixedleg-frequency').val();
	var FixedlegFrequencyMultiplier = parseFloat($('#bo-irs-fixedleg-multiplier').val());
	date = $('#bo-irs-maturity').val().split('/');
	var Maturity = parseInt(date[2] + date[0] + date[1]);					
	if (Maturity < Tradedate){			
		window.checkFormat = false;
		body = errLocalObject;
		body.error = DATE_RELATION_ERROR;
		body.message = "Maturity has to be a date after Trade Date"
		throw body
		return;
	}							   
	var FixedlegFixedrate = parseFloat($('#bo-irs-fixedleg-rate').val());
	var FloatinglegIndex = $('#bo-irs-floatingleg-index').val();
	var FloatinglegFixingcalendar = $('#bo-irs-floatingleg-fixingCalendar').val();
	var FloatinglegInitCapital = parseFloat($('#bo-irs-floatingleg-initCapital').val());
	var FloatinglegInitCapitalcurr = $('#bo-irs-floatingleg-initCapitalCurr').val();
	var FloatinglegRateconvention = $('#bo-irs-floatingleg-rateConvention').val();
	var FloatinglegPaymentcalendar = $('#bo-irs-floatingleg-paymentCalendar').val();
	var FloatinglegFrequencyUnit = $('#bo-irs-floatingleg-frequency').val();
	var FloatinglegFrequencyMultiplier = parseFloat($('#bo-irs-floatingleg-multiplier').val());				
	var data = [
		{
			"userName": "USER01",
			"party": "TMB Bangkok",
			"counterParty": Counterparty,
			"portfolio": Portfolio,
			"tradeHeader": {
				"tradeDate": Tradedate,
				"tradeDestination": "external"
			},
			"tradeBody": {
				"interestRateSwap": {
					"templateLabel": Template,
					"fixedRatePayerReference": Fixedrate,
					"floatingRatePayerReference": Floatingrate,
					"stubPeriodPosition": stubperiod,
					"fixedStream": {
						"initialCapitalAmount": FixedlegInitCapital,
						"initialCapitalCurrency": FixedlegInitCapitalCurr,
						"rateConvention": FixedlegRateconvention,
						"paymentCalendar": FixedlegPaymentcalendar,
						"frequency": {
							"periodUnit": FixedlegFrequencyUnit,
							"periodMultiplier": FixedlegFrequencyMultiplier
						},
						"maturity": Maturity,
						"fixedRate": FixedlegFixedrate
					},
					"floatingStream": {
						"floatingRateStreamTemplate": FloatinglegIndex,
						"fixingCalendar": FloatinglegFixingcalendar,
						"initialCapitalAmount": FloatinglegInitCapital,
						"initialCapitalCurrency": FloatinglegInitCapitalcurr,
						"rateConvention": FloatinglegRateconvention,
						"paymentCalendar": FloatinglegPaymentcalendar,
						"frequency": {
							"periodUnit": FloatinglegFrequencyUnit,
							"periodMultiplier": FloatinglegFrequencyMultiplier
						},
						"maturity": Maturity
					}
				}
			}
		}
	];
	return data;
}
function bookDepo(){
	checkFormat = true;
	var Counterparty = $('#bo-depo-counterParty').val();
	var Portfolio = $('#bo-depo-portfolio').val();
	var date = $('#bo-depo-tradeDate').val().split('/');
	var Tradedate = parseInt(date[2] + date[0] + date[1]);
	date = $('#bo-depo-stream-startperiod').val().split('/');
	var StartPeriod = parseInt(date[2] + date[0] + date[1]);
	date = $('#bo-depo-stream-matperiod').val().split('/');
	var MaturityPeriod = parseInt(date[2] + date[0] + date[1]); 
	var Payout = $('#bo-depo-payout').val();
	var Stubperiod = $('#bo-depo-stubPeriod').val();
	var Nominal = parseFloat($('#bo-depo-nominal').val());
	var Currency = $('#bo-depo-currency').val();
	var Rateconvention = $('#bo-depo-stream-rateConvention').val();
	var FrequencyUnit = $('#bo-depo-stream-frequency').val();
	var FrequencyMultiplier= parseFloat($('#bo-depo-stream-multiplier').val());
	var Rate = parseFloat($('#bo-depo-stream-rate').val());
	if (MaturityPeriod < StartPeriod){			
			window.checkFormat = false;
			body = errLocalObject;
			body.error = DATE_RELATION_ERROR;
			body.message = "Maturity Period has to be a date after Start Period"
			throw body;
			return;
	}		
	var data = [
				  	{
				   		"userName": "USER01",
				    	"party": "TMB Bangkok",
				   		"counterParty": Counterparty, 
				    	"portfolio": Portfolio,
				   		"tradeHeader": {
				      		"tradeDate": Tradedate,
				     		"tradeDestination": "external"
				    	},
			    		"tradeBody": {
				      		"loanDeposit": {
			        			"payOut": Payout,
			       				"stubPeriodPosition": Stubperiod,
			        			"Stream": {
			          				"initialCapitalAmount": Nominal,
			          				"initialCapitalCurrency": Currency,
			          				"rateConvention": Rateconvention,
			          				"frequency": {
			            				"periodUnit": FrequencyUnit,
							           	"periodMultiplier": FrequencyMultiplier
									},
			          				"effectiveDate": StartPeriod,
			          				"maturity": MaturityPeriod,
			          				"rate": Rate
		        				}
			      			}
			    		}
				  	}
				]

	return data;
	
}
function bookSpot() {
	checkFormat = true;
	var Counterparty = $('#bo-sf-counterParty').val();
	var Portfolio = $('#bo-sf-portfolio').val();
	var date = $('#bo-sf-tradeDate').val().split('/');
	var Tradedate = parseInt(date[2] + date[0] + date[1]);
	var time = $('#bo-sf-timeToMaturity').val().split('/');
	var TimetoMaturity = parseInt(time[2] + time[0] + time[1]);

	if (TimetoMaturity < Tradedate){			
		window.checkFormat = false;
	return "maturity date < trade date";
	}

	var BuyCurrency = $('#bo-sf-buyCurrency').val();
	var SellCurrency = $('#bo-sf-sellCurrency').val();
	var Amountbuycurr = parseInt($('#bo-sf-amountBuyCurr').val());
	var Netrate = parseInt($('#bo-sf-netRate').val());
	var Margin = parseInt($('#bo-sf-margin').val());
	var FXSpotRate = parseInt($('#bo-sf-fxSpotRate').val());
	// parseInt($('#bo-sf-fxSpotMargin').val());
	var FXSpotMargin = 0;
	var NonDeliverable = ($('#bo-sf-nonDeliverable').val() == 'True') ? true : false;
	var data = [
		{
			"userName": "USER01",
			"party": "TMB Bangkok",
			"counterParty": Counterparty,
			"portfolio": Portfolio,
			"tradeHeader": {
				"tradeDate": Tradedate,
				"tradeDestination": "external"
			},
			"tradeBody": {
				"fxSpotForward": {
					"date": TimetoMaturity,
					"buyCurrency": BuyCurrency,
					"sellCurrency": SellCurrency,
					"amountBuyCurrency": Amountbuycurr,
					"netRate": Netrate,
					"margin": Margin,
					"fxSpotRate": FXSpotRate,
					"fxSpotMargin": FXSpotMargin,
					"nonDeliverable": NonDeliverable,
					"fxRateSourceLabel": "BOT01"
				}
			}
		}
	];
	return data;
}
function bookFXSwap() {
	checkFormat = true;
	var Counterparty = $('#bo-fx-counterParty').val();
	var Portfolio = $('#bo-fx-portfolio').val();
	var date = $('#bo-fx-tradeDate').val().split('/');
	var Tradedate = parseInt(date[2] + date[0] + date[1]);
	var startDate = $('#bo-fx-startDate').val().split('/');
	var StartDate = parseInt(startDate[2] + startDate[0] + startDate[1]);
	var maturityDate = $('#bo-fx-maturityDate').val().split('/');
	var MaturityDate = parseInt(maturityDate[2] + maturityDate[0] + maturityDate[1]);
	var BuyCurrency1 = $('#bo-fx-buyCurrency1').val();
	var SellCurrency1 = $('#bo-fx-sellCurrency1').val();
	var Amountbuycurr1 = parseFloat($('#bo-fx-amountBuyCurr1').val());
	var Netrate1 = parseFloat($('#bo-fx-netRate1').val());
	var Margin1 = parseFloat($('#bo-fx-margin1').val());
	var BuyCurrency2 = $('#bo-fx-buyCurrency2').val();
	var SellCurrency2 = $('#bo-fx-sellCurrency2').val();
	var Amountbuycurr2 = parseFloat($('#bo-fx-amountBuyCurr2').val());
	var Netrate2 = parseFloat($('#bo-fx-netRate2').val());
	var Margin2 = parseFloat($('#bo-fx-margin2').val());
	var FowardDelivery = $('#bo-fx-fowardDelivery').val();
	var NonDeliverable = ($('#bo-fx-nonDeliverable').val() == 'True') ? true : false;
	var data = [
		{
			"userName": "USER01",
			"party": "TMB Bangkok",
			"counterParty": Counterparty,
			"portfolio": Portfolio,
			"tradeHeader": {
				"tradeDate": Tradedate,
				"tradeDestination": "external"
			},
			"tradeBody": {
				"forexSwapLeg1": {
					"date": StartDate,
					"sellCurrency": SellCurrency1,
					"buyCurrency": BuyCurrency1,
					"amountBuyCurrency": Amountbuycurr1,
					"netRate": Netrate1,
					"margin": Margin1,
					"forwardDelivery": FowardDelivery
				},
				"forexSwapLeg2": {
					"date": MaturityDate,
					"sellCurrency": SellCurrency2,
					"buyCurrency": BuyCurrency2,
					"amountBuyCurrency": Amountbuycurr2,
					"netRate": Netrate2,
					"margin": Margin2,
					"nonDeliverable": NonDeliverable,
					"fxRateSourceLabel": "BOT01"
				}
			}
		}
	];
	console.log(data);
	return data;
}