function exportDeal() {
	$('.loader-ball').addClass('is-active');
	var baseUri = 'http://murex-trade-repository-api-2-v2.cloudhub.io/api/';
	var type = $('#ex-type').val();	
	var path = (type == 'Package Number') ? 'package' : 'trade';
	//(window.optionDeal == 'B2B') ? 'package' : 'trade';///CURR/FXD/forexSwap';
	var link = baseUri + path + '?ids=' + $('#ex-id').val();	
	console.log(link);				
	$.ajax({
		url: link,
		type: "GET",
		//async: false,
		dataType: "json",
		success: function (data) {	
			console.log(data);
			try{
				if (data[0].tradeBody.bond != null)
					exportBond(data[0]);
				if (data[0].tradeBody.interestRateSwap != null)
					exportIRS(data[0]);
				if (data[0].tradeBody.fxSpotForward != null)
					exportSpot(data[0]);
				if (data[0].tradeBody.loanDeposit != null)
					exportDepo(data[0]);
				if (data[0].tradeBody.forexSwap != null)
					exportFXSwap(data[0]);
				if (data.length > 1){					
					exportB2B(data);					
				}
				$('.loader-ball').removeClass('is-active');
			}catch(exception){
				errorNoti("Export","Wrong ID");
			}
		},
		error: function(){			
			errorNoti("export");
		}
	});
}

function exportBond(data) {
	$('#ex-bond-counterParty').val(data.counterParty);
	$('#ex-bond-portfolio').val(data.portfolio);

	//date export
	$('#ex-bond-tradeDate').val(correctDateString(data.tradeHeader.tradeDate,"/"));
	$('#ex-bond-action').val(data.tradeBody.bond.action);
	$('#ex-bond-security').val(data.tradeBody.bond.securityDisplayLabel);
	$('#ex-bond-yield').val(data.tradeBody.bond.tradeYield);
	$('#ex-bond-quantity').val(data.tradeBody.bond.quantity);
	$('#ex-bond-nominal').val(data.tradeBody.bond.nominalAmount);
	$('#ex-bond-cleanPrice').val(data.tradeBody.bond.cleanPrice);
	displayExport('#exportBond');
}
function exportIRS(data) {
	console.log(data);
	$('#ex-irs-counterParty').val(data.counterParty);
	$('#ex-irs-portfolio').val(data.portfolio);
	$('#ex-irs-tradeDate').val(correctDateString(data.tradeHeader.tradeDate,"/"));
	$('#ex-irs-template').val(data.tradeBody.interestRateSwap.templateLabel);
	
	$('#ex-irs-fixedRate').val(data.tradeBody.interestRateSwap.fixedRatePayerReference);					
	$('#ex-irs-floatingRate').val(data.tradeBody.interestRateSwap.floatingRatePayerReference);					
	$('#ex-irs-stubPeriod').val(data.tradeBody.interestRateSwap.stubPeriodPosition);										
	$('#ex-irs-fixedleg-initCapital').val(data.tradeBody.interestRateSwap.fixedStream.initialCapitalAmount);
	$('#ex-irs-fixedleg-initCapitalCurr').val(data.tradeBody.interestRateSwap.fixedStream.initialCapitalCurrency);					
	$('#ex-irs-fixedleg-rateConvention').val(data.tradeBody.interestRateSwap.fixedStream.rateConvention);
	$('#ex-irs-fixedleg-paymentCalendar').val(data.tradeBody.interestRateSwap.fixedStream.paymentCalendar);
	
	$('#ex-irs-fixedleg-frequency').val(data.tradeBody.interestRateSwap.fixedStream.frequency.periodUnit);
	$('#ex-irs-fixedleg-multiplier').val(data.tradeBody.interestRateSwap.fixedStream.frequency.periodMultiplier);
	
	$('#ex-irs-maturity').val(correctDateString(data.tradeBody.interestRateSwap.fixedStream.maturity,"/"));					
	$('#ex-irs-fixedleg-rate').val(data.tradeBody.interestRateSwap.fixedStream.fixedRate);
	$('#ex-irs-floatingleg-index').val(data.tradeBody.interestRateSwap.floatingStream.floatingRateStreamTemplate);
	$('#ex-irs-floatingleg-fixingCalendar').val(data.tradeBody.interestRateSwap.floatingStream.fixingCalendar);
	$('#ex-irs-floatingleg-initCapital').val(data.tradeBody.interestRateSwap.floatingStream.initialCapitalAmount);
	$('#ex-irs-floatingleg-initCapitalcurr').val(data.tradeBody.interestRateSwap.floatingStream.initialCapitalCurrency);
	$('#ex-irs-floatingleg-rateConvention').val(data.tradeBody.interestRateSwap.floatingStream.rateConvention);
	$('#ex-irs-floatingleg-paymentCalendar').val(data.tradeBody.interestRateSwap.floatingStream.paymentCalendar);
	$('#ex-irs-floatingleg-frequency').val(data.tradeBody.interestRateSwap.floatingStream.frequency.periodUnit);
	$('#ex-irs-floatingleg-multipier').val(data.tradeBody.interestRateSwap.floatingStream.frequency.periodMultiplier);		
	displayExport('#exportIRS');		
}
function exportDepo(data) {
	console.log(data);
	$('#ex-depo-counterParty').val(data.counterParty);
	$('#ex-depo-portfolio').val(data.portfolio);
	$('#ex-depo-tradeDate').val(correctDateString(data.tradeHeader.tradeDate,"/"));
	$('#ex-depo-payout').val(data.tradeBody.loanDeposit.payOut);
	$('#ex-depo-stubPeriod').val(data.tradeBody.loanDeposit.stubPeriodPosition);
	$('#ex-depo-nominal').val(data.tradeBody.loanDeposit.Stream.initialCapitalAmount);
	$('#ex-depo-currency').val(data.tradeBody.loanDeposit.Stream.initialCapitalCurrency);
	$('#ex-depo-stream-rateConvention').val(data.tradeBody.loanDeposit.Stream.rateConvention);
	$('#ex-depo-stream-rate').val(data.tradeBody.loanDeposit.Stream.rate);
	$('#ex-depo-stream-startperiod').val(correctDateString(data.tradeBody.loanDeposit.Stream.effectiveDate,"/"));
	$('#ex-depo-stream-matperiod').val(correctDateString(data.tradeBody.loanDeposit.Stream.maturity,"/"));
	$('#ex-depo-stream-frequency').val(data.tradeBody.loanDeposit.Stream.frequency.periodUnit);
	$('#ex-depo-stream-multiplier').val(data.tradeBody.loanDeposit.Stream.frequency.periodMultiplier);
	displayExport('#exportDepo');		
}
function exportSpot(data) {
	console.log(data);
	$('#ex-sf-counterParty').val(data.counterParty);
	$('#ex-sf-portfolio').val(data.portfolio);
	$('#ex-sf-tradeDate').val(correctDateString(data.tradeHeader.tradeDate,"/"));
	$('#ex-sf-timeToMaturity').val(correctDateString(data.tradeBody.fxSpotForward.date,"/"));
	$('#ex-sf-buyCurrency').val(data.tradeBody.fxSpotForward.buyCurrency);
	$('#ex-sf-sellCurrency').val(data.tradeBody.fxSpotForward.sellCurrency);
	$('#ex-sf-amountBuyCurr').val(data.tradeBody.fxSpotForward.amountBuyCurrency);
	$('#ex-sf-netRate').val(data.tradeBody.fxSpotForward.netRate);
	$('#ex-sf-margin').val(data.tradeBody.fxSpotForward.margin);
	$('#ex-sf-fxSpotRate').val(data.tradeBody.fxSpotForward.fxSpotRate);
	$('#ex-sf-fxSpotMargin').val(data.tradeBody.fxSpotForward.fxSpotMargin);
	$('#ex-sf-nonDeliverable').val(data.tradeBody.fxSpotForward.nonDeliverable);
	displayExport('#exportSpot');		
}
function exportFXSwap(data) {
	console.log(data);
	$('#ex-fxs-counterParty').val(data.counterParty);
	$('#ex-fxs-portfolio').val(data.portfolio);
	$('#ex-fxs-tradeDate').val(correctDateString(data.tradeHeader.tradeDate,"/"));
	$('#ex-fx-startDate').val(correctDateString(data.tradeBody.forexSwap.forexSwapLeg1.date,"/"));
	$('#ex-fx-buyCurrency1').val(data.tradeBody.forexSwap.forexSwapLeg1.buyCurrency);
	$('#ex-fx-sellCurrency1').val(data.tradeBody.forexSwap.forexSwapLeg1.sellCurrency);
	$('#ex-fx-amountBuyCurr1').val(data.tradeBody.forexSwap.forexSwapLeg1.amountBuyCurrency);
	$('#ex-fx-netRate1').val(data.tradeBody.forexSwap.forexSwapLeg1.netRate);
	$('#ex-fx-margin1').val(data.tradeBody.forexSwap.forexSwapLeg1.margin);
	$('#ex-fx-fowardDelivery').val(data.tradeBody.forexSwap.forexSwapLeg1.forwardDelivery);
	$('#ex-fx-maturityDate').val(correctDateString(data.tradeBody.forexSwap.forexSwapLeg2.date,"/"));
	$('#ex-fx-buyCurrency2').val(data.tradeBody.forexSwap.forexSwapLeg2.buyCurrency);
	$('#ex-fx-sellCurrency2').val(data.tradeBody.forexSwap.forexSwapLeg2.sellCurrency);
	$('#ex-fx-amountBuyCurr2').val(data.tradeBody.forexSwap.forexSwapLeg2.amountBuyCurrency);
	$('#ex-fx-netRate2').val(data.tradeBody.forexSwap.forexSwapLeg2.netRate);
	$('#ex-fx-margin2').val(data.tradeBody.forexSwap.forexSwapLeg2.margin);
	$('#ex-fx-nonDeliverable').val(data.tradeBody.forexSwap.forexSwapLeg2.nonDeliverable);	
	displayExport('#exportFXSwap');		
}
function exportB2B(data){
	var result = '';
	for (i=1; i<data.length; i++){
		result += '<div class="2u 12u(mobilep)" style="padding-top:0px"><h4>B2B Portfolio ' + i + '</h4></div><div class="4u 12u(mobilep)" style="padding-top:0px"><input type="text" value="' + data[i].portfolio + '"readonly></div>';
	}
	$('#ex-b2bdeal').css('display', 'block');
	$('#ex-b2bportfolio').css('display', 'block');
	$('#ex-b2bportfolio').html(result);
	//document.getElementById("ex-b2bdeal").style.display = "block";
	//document.getElementById("ex-b2bportfolio").style.display = "block";
}
function displayExport(id){
	var divArray = ['#exportBond','#exportIRS','#exportDepo','#exportSpot','#exportFXSwap', '#ex-b2bdeal', '#ex-b2bportfolio'];
	for (i = 0; i < divArray.length; i++){
		if (id == divArray[i])
			$(divArray[i]).css('display', 'block');
		else
			$(divArray[i]).css('display', 'none');
	}
}