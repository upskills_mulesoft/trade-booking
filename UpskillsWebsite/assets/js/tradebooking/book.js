function calculatesfAmountBuyCurr(){
	var net = $("#bo-sf-netRate").val() != "" ? $("#bo-sf-netRate").val() : 0;
	var amount = $("#bo-sf-amountBuyCurr").val() != "" ? $("#bo-sf-amountBuyCurr").val() : 0;
	$("#bo-sf-amountBuyCurr2").val(net*amount);
}

function toggleItem(name){
	$("#"+name+"Txt").fadeIn(1000);
	$("#"+name+"Val").fadeIn(1000);
	var contract = document.createElement('span');
    document.getElementById('bo-sf-name').innerHTML = "";

	if (name == "forwardDeal"){
		$("#spotDealTxt").fadeOut(1000);
		$("#spotDealVal").fadeOut(1000);
		$("#bo-sf-fxSpotRate").val(0);
		contract.innerHTML = "Forward Deal";
	}
	else if (name == "spotDeal"){
		$("#forwardDealTxt").fadeOut(1000);
		$("#forwardDealVal").fadeOut(1000);
		$("#bo-sf-fxSwapPoint").val(0);
		contract.innerHTML = "Spot Deal";
	}
    document.getElementById('bo-sf-name').appendChild(contract);
}

function calculatesfAmountBuyCurr(){
	var net = $("#bo-sf-netRate").val() != "" ? $("#bo-sf-netRate").val() : 0;
	var amount = $("#bo-sf-amountBuyCurr").val() != "" ? $("#bo-sf-amountBuyCurr").val() : 0;
	$("#bo-sf-amountBuyCurr2").val((net*amount).toFixed(4));
}


function calculateMargin(){
	var spot = $("#bo-sf-fxSpotMargin").val() != "" ? $("#bo-sf-fxSpotMargin").val() : 0;
	var swap = $("#bo-sf-fxSwapMargin").val() != "" ? $("#bo-sf-fxSwapMargin").val() : 0;
	$("#bo-sf-margin").val((parseFloat(spot)+parseFloat(swap)).toFixed(4));
}

function getDealType(){
	var x = new Date($("#bo-sf-timeToMaturity").val());
	var y = new Date($("#bo-sf-tradeDate").val());
	var newdate = new Date(y);
	newdate.setDate(newdate.getDate()+2);
	var nd = new Date(newdate);

	if (x.getTime() > nd.getTime()){
		toggleItem('forwardDeal');

		var spot = $("#bo-sf-fxSpotRate").val() != "" ? $("#bo-sf-fxSpotRate").val() : 0;
		var swap = $("#bo-sf-fxSwapPoint").val() != "" ? $("#bo-sf-fxSwapPoint").val() : 0;
		calculateMargin();
		var margin = $("#bo-sf-margin").val() != "" ? $("#bo-sf-margin").val() : 0;

		var temp = parseFloat(spot) + (parseFloat(swap)/100);
		temp += (parseFloat(margin)/100);
		$("#bo-sf-netRate").val(temp);

		var contract = document.createElement('span');
		document.getElementById('bo-sf-name').innerHTML = "";
		contract.innerHTML = "Forward Deal";
		document.getElementById('bo-sf-name').appendChild(contract);
	}
	else if (x.getTime() === nd.getTime()){
		toggleItem('spotDeal');
		// toggleItem('spotDealVal');

		var spot = $("#bo-sf-fxSpotRate").val() != "" ? $("#bo-sf-fxSpotRate").val() : 0;
		var swap = $("#bo-sf-fxSwapPoint").val() != "" ? $("#bo-sf-fxSwapPoint").val() : 0;
		calculateMargin();
		var margin = $("#bo-sf-margin").val() != "" ? $("#bo-sf-margin").val() : 0;
	}
	else {
		document.getElementById('bo-sf-name').innerHTML = "";
	 	errorNoti('assign a date in the','Invalid Date');
	}


}

function calculateNetRate()
{
	var spot = $("#bo-sf-fxSpot").val() != "" ? $("#bo-sf-fxSpot").val() : 0;
	var swap = $("#bo-sf-fxSwapPoint").val() != "" ? $("#bo-sf-fxSwapPoint").val() : 0;
	calculateMargin();
	var margin = $("#bo-sf-margin").val() != "" ? $("#bo-sf-margin").val() : 0;

	var temp = parseFloat(spot) + (parseFloat(swap)/100);
	temp += (parseFloat(margin)/100);
	$("#bo-sf-netRate").val(temp.toFixed(4));	
}

function IRSfixedRateChange()
{
	var fix = document.getElementById("bo-irs-fixedRate").value;
	var float = document.getElementById("bo-irs-floatingRate");
	if(fix == 1){
		float.value = 2;
	}
	else{
		float.value = 1;
	}
}

function IRSfloatingRateChange()
{
	var fix = document.getElementById("bo-irs-fixedRate");
	var float = document.getElementById("bo-irs-floatingRate").value;
	if(float == 1){
		fix.value = 2;
	}
	else{
		fix.value = 1;
	}
}			

function bookDeal() {
	$('.loader-ball').addClass('is-active');
	var baseUri = 'http://murex-trade-repository-api-2-v2.cloudhub.io/api/';
	var path = //(window.optionDeal == 'Bond') ? 'trade/IRD/bond' : 
	// (window.optionDeal == 'IRS') ? 'trade/IRD/interestRateSwap' : 
	// (window.optionDeal == 'Spot') ? 'trade/CURR/FXD/spotForward' : 
	// (window.optionDeal == 'Depo') ? 'trade/IRD/loanDeposit' : 
	(window.optionDeal == 'B2B') ? 'package' : 'trade'///CURR/FXD/forexSwap';
	var body = null;
	
	try {
		body = 	(window.optionDeal == 'Bond') ? bookBond() : 
		(window.optionDeal == 'IRS') ? bookIRS() : 
		(window.optionDeal == 'Spot') ? bookSpot() : 
		(window.optionDeal == 'Depo') ? bookDepo() :
		(window.optionDeal == 'B2B') ? bookB2B(bookSpot()[0]) : bookFXSwap();
	} catch (exception){
		console.log(exception);
		errorNoti ("book",exception.message);
		return;
	}		

	console.log(body);
	var link = baseUri + path;
	if (checkFormat){
		$.ajax({
			url: link,
			type: "POST",
				//async: false,
				dataType: "json",
				data: JSON.stringify(body),
				contentType:"application/json; charset=utf-8",
				success: function (data) {
					console.log(data);
					successMessage(data);
					$('.loader-ball').removeClass('is-active');
				},
				error: function(errObj){		
					errorNoti("book", errorStringCreate(errObj));	  
				}
			});
	}																 
	else errorNoti("book", "There exists invalid fields, please fix it and try again!");											  
}





function bookBond() {
	checkFormat = true;
	var Counterparty = $('#bo-bond-counterParty').val();
	var Portfolio = $('#bo-bond-portfolio').val();
	var date = $('#bo-bond-tradeDate').val().split('/');
	var Tradedate = parseInt(date[2] + date[0] + date[1]);
	var buysell = $('#bo-bond-action').val().toLowerCase();
	var Security = $('#bo-bond-security').val();			  
	var Yield = parseFloat($('#bo-bond-yield').val());
	var Quantity = parseFloat($('#bo-bond-quantity').val());
	var Nominal = parseFloat($('#bo-bond-nominal').val());
	var Cleanprice = parseFloat($('#bo-bond-cleanPrice').val());
	var AccrualCoupon = parseFloat($('#bo-bond-accrualcoup').val());
	var	FlatSettlement = parseFloat($('#bo-bond-flatsettle').val());
	var	DirtyPrice = parseFloat($('#bo-bond-dirtyprice').val());						   
	var data = [
	{
		"userName": "USER01",
		"party": "TMB Bangkok",
		"counterParty": Counterparty,
		"portfolio": Portfolio,
		"tradeHeader": {
			"tradeDate": Tradedate,
			"tradeDestination": "external"
		},
		"tradeBody": {
			"bond": {
				"action": buysell,
				"securityMarket": "USD_GOVT",
				"securityDisplayLabel": Security,
				"currency": "USD",
				"tradeYield": Yield,
				"quantity": Quantity,
				"nominalAmount": Nominal,
				"cleanPrice": Cleanprice
			}
		}
	}
	];
	return data;
}
function bookIRS() {
	checkFormat = true;	
	var Counterparty = $('#bo-irs-counterParty').val();
	var Portfolio = $('#bo-irs-portfolio').val();
	var date = $('#bo-irs-tradeDate').val().split('/');
	var Tradedate = parseInt(date[2] + date[0] + date[1]);
	var Template = $('#bo-irs-template').val();
	var Fixedrate = $('#bo-irs-fixedRate').val();
	var stubperiod = $('#bo-irs-stubPeriod').val();
	var Floatingrate = $('#bo-irs-floatingRate').val();
	var FixedlegInitCapital = parseFloat($('#bo-irs-fixedleg-initCapital').val());
	var FixedlegInitCapitalCurr = $('#bo-irs-fixedleg-initCapitalCurr').val();
	var FixedlegRateconvention = $('#bo-irs-fixedleg-rateConvention').val();
	var FixedlegPaymentcalendar = $('#bo-irs-fixedleg-paymentCalendar').val();
	var FixedlegFrequencyUnit = $('#bo-irs-fixedleg-frequency').val();
	var FixedlegFrequencyMultiplier = parseFloat($('#bo-irs-fixedleg-multiplier').val());
	date = $('#bo-irs-maturity').val().split('/');
	var Maturity = parseInt(date[2] + date[0] + date[1]);					
	if (Maturity < Tradedate){			
		window.checkFormat = false;
		body = errLocalObject;
		body.error = DATE_RELATION_ERROR;
		body.message = "Maturity has to be a date after Trade Date"
		throw body
		return;
	}							   
	var FixedlegFixedrate = parseFloat($('#bo-irs-fixedleg-rate').val());
	var FloatinglegIndex = $('#bo-irs-floatingleg-index').val();
	var FloatinglegFixingcalendar = $('#bo-irs-floatingleg-fixingCalendar').val();
	var FloatinglegInitCapital = parseFloat($('#bo-irs-floatingleg-initCapital').val());
	var FloatinglegInitCapitalcurr = $('#bo-irs-floatingleg-initCapitalCurr').val();
	var FloatinglegRateconvention = $('#bo-irs-floatingleg-rateConvention').val();
	var FloatinglegPaymentcalendar = $('#bo-irs-floatingleg-paymentCalendar').val();
	var FloatinglegFrequencyUnit = $('#bo-irs-floatingleg-frequency').val();
	var FloatinglegFrequencyMultiplier = parseFloat($('#bo-irs-floatingleg-multiplier').val());			
	var data = [
	{
		"userName": "USER01",
		"party": "TMB Bangkok",
		"counterParty": Counterparty,
		"portfolio": Portfolio,
		"tradeHeader": {
			"tradeDate": Tradedate,
			"tradeDestination": "external"
		},
		"tradeBody": {
			"interestRateSwap": {
				"templateLabel": Template,
				"fixedRatePayerReference": Fixedrate,
				"floatingRatePayerReference": Floatingrate,
				"stubPeriodPosition": stubperiod,
				"fixedStream": {
					"initialCapitalAmount": FixedlegInitCapital,
					"initialCapitalCurrency": FixedlegInitCapitalCurr,
					"rateConvention": FixedlegRateconvention,
					"paymentCalendar": FixedlegPaymentcalendar,
					"frequency": {
						"periodUnit": FixedlegFrequencyUnit,
						"periodMultiplier": FixedlegFrequencyMultiplier
					},
					"maturity": Maturity,
					"fixedRate": FixedlegFixedrate
				},
				"floatingStream": {
					"floatingRateStreamTemplate": FloatinglegIndex,
					"fixingCalendar": FloatinglegFixingcalendar,
					"initialCapitalAmount": FloatinglegInitCapital,
					"initialCapitalCurrency": FloatinglegInitCapitalcurr,
					"rateConvention": FloatinglegRateconvention,
					"paymentCalendar": FloatinglegPaymentcalendar,
					"frequency": {
						"periodUnit": FloatinglegFrequencyUnit,
						"periodMultiplier": FloatinglegFrequencyMultiplier
					},
					"maturity": Maturity
				}
			}
		}
	}
	];
	return data;
}
function bookDepo(){
	checkFormat = true;
	var Counterparty = $('#bo-depo-counterParty').val();
	var Portfolio = $('#bo-depo-portfolio').val();
	var date = $('#bo-depo-tradeDate').val().split('/');
	var Tradedate = parseInt(date[2] + date[0] + date[1]);
	date = $('#bo-depo-stream-startperiod').val().split('/');
	var StartPeriod = parseInt(date[2] + date[0] + date[1]);
	date = $('#bo-depo-stream-matperiod').val().split('/');
	var MaturityPeriod = parseInt(date[2] + date[0] + date[1]); 
	var Payout = $('#bo-depo-payout').val();
	var Stubperiod = $('#bo-depo-stubPeriod').val();
	var Nominal = parseFloat($('#bo-depo-nominal').val());
	var Currency = $('#bo-depo-currency').val();
	var Rateconvention = $('#bo-depo-stream-rateConvention').val();
	var FrequencyUnit = $('#bo-depo-stream-frequency').val();
	var FrequencyMultiplier= parseFloat($('#bo-depo-stream-multiplier').val());
	var Rate = parseFloat($('#bo-depo-stream-rate').val());
	if (MaturityPeriod < StartPeriod){			
		window.checkFormat = false;
		body = errLocalObject;
		body.error = DATE_RELATION_ERROR;
		body.message = "Maturity Period has to be a date after Start Period"
		throw body;
		return;
	}		
	var data = [
	{
		"userName": "USER01",
		"party": "TMB Bangkok",
		"counterParty": Counterparty, 
		"portfolio": Portfolio,
		"tradeHeader": {
			"tradeDate": Tradedate,
			"tradeDestination": "external"
		},
		"tradeBody": {
			"loanDeposit": {
				"payOut": Payout,
				"stubPeriodPosition": Stubperiod,
				"Stream": {
					"initialCapitalAmount": Nominal,
					"initialCapitalCurrency": Currency,
					"rateConvention": Rateconvention,
					"frequency": {
						"periodUnit": FrequencyUnit,
						"periodMultiplier": FrequencyMultiplier
					},
					"effectiveDate": StartPeriod,
					"maturity": MaturityPeriod,
					"rate": Rate
				}
			}
		}
	}
	];

	return data;
	
}



function bookB2B(oriDeal){
	var B2BPortfolio = $("#bo-sf-b2bportfolio").val();
	var data = [
		[
			oriDeal
			,
			{
				"userName": "USER01",
				"party": "TMB Bangkok",
				"counterParty": oriDeal.party,
				"portfolio": B2BPortfolio,
				"counterPortfolio":  oriDeal.portfolio,
				"tradeHeader": {
					"tradeDate": oriDeal.tradeHeader.tradeDate,
					"tradeDestination": "internal"
				},
				"tradeBody": {
					"fxSpotForward": {
						"date": oriDeal.tradeBody.fxSpotForward.date,
						"sellCurrency": oriDeal.tradeBody.fxSpotForward.sellCurrency,
						"buyCurrency": oriDeal.tradeBody.fxSpotForward.buyCurrency,
						"amountBuyCurrency": oriDeal.tradeBody.fxSpotForward.amountBuyCurrency,
						"netRate": oriDeal.tradeBody.fxSpotForward.netRate,
						"margin": 0,
						"fxSpotRate": oriDeal.tradeBody.fxSpotForward.fxSpotRate,
						"fxSpotMargin": 0,
						"nonDeliverable": oriDeal.tradeBody.fxSpotForward.nonDeliverable,
						"fxRateSourceLabel": null
					}
				}
			}
		]
	];
	spotForwardOriDeal = null;
	return data;
}

function bookSpot() {
	checkFormat = true;
	var Counterparty = $('#bo-sf-counterParty').val();
	var Portfolio = $('#bo-sf-portfolio').val();
	var date = $('#bo-sf-tradeDate').val().split('/');
	var Tradedate = parseInt(date[2] + date[0] + date[1]);
	var time = $('#bo-sf-timeToMaturity').val().split('/');
	var TimetoMaturity = parseInt(time[2] + time[0] + time[1]);

	if (TimetoMaturity < Tradedate){			
		window.checkFormat = false;
		body = errLocalObject;
		body.error = DATE_RELATION_ERROR;
		body.message = "Maturity has to be a date after Tradedate"
		throw body;
		return;
	}

	var BuyCurrency = $('#bo-sf-buyCurrency').val();
	var SellCurrency = $('#bo-sf-sellCurrency').val();
	var Amountbuycurr = parseInt($('#bo-sf-amountBuyCurr').val());
	var Netrate = parseFloat($('#bo-sf-netRate').val());
	var Margin = parseFloat($('#bo-sf-margin').val());
	var FXSpotRate = parseFloat($('#bo-sf-fxSpotRate').val());
	// parseInt($('#bo-sf-fxSpotMargin').val());
	var FXSpotMargin = 0;
	var NonDeliverable = ($('#bo-sf-nonDeliverable').val() == 'True') ? true : false;
	var data = [
	{
		"userName": "USER01",
		"party": "TMB Bangkok",
		"counterParty": Counterparty,
		"portfolio": Portfolio,
		"tradeHeader": {
			"tradeDate": Tradedate,
			"tradeDestination": "external"
		},
		"tradeBody": {
			"fxSpotForward": {
				"date": TimetoMaturity,
				"buyCurrency": BuyCurrency,
				"sellCurrency": SellCurrency,
				"amountBuyCurrency": Amountbuycurr,
				"netRate": Netrate,
				"margin": Margin,
				"fxSpotRate": FXSpotRate,
				"fxSpotMargin": FXSpotMargin,
				"nonDeliverable": NonDeliverable,
				"fxRateSourceLabel": "BOT01"
			}
		}
	}
	];


	// var isB2B = $('#optionB2B').prop('checked');
	// if (isB2B) {
	// 	window.optionDeal = "B2B"
	// 	spotForwardOriDeal = data;
	// 	bookDeal();
	// };
	return data;
}
function bookFXSwap() {
	checkFormat = true;
	var Counterparty = $('#bo-fx-counterParty').val();
	var Portfolio = $('#bo-fx-portfolio').val();
	var date = $('#bo-fx-tradeDate').val().split('/');
	var Tradedate = parseInt(date[2] + date[0] + date[1]);
	var startDate = $('#bo-fx-startDate').val().split('/');
	var StartDate = parseInt(startDate[2] + startDate[0] + startDate[1]);
	var maturityDate = $('#bo-fx-maturityDate').val().split('/');
	var MaturityDate = parseInt(maturityDate[2] + maturityDate[0] + maturityDate[1]);
	var BuyCurrency1 = $('#bo-fx-buyCurrency1').val();
	var SellCurrency1 = $('#bo-fx-sellCurrency1').val();
	var Amountbuycurr1 = parseFloat($('#bo-fx-amountBuyCurr1').val());
	var Netrate1 = parseFloat($('#bo-fx-netRate1').val());
	var Margin1 = parseFloat($('#bo-fx-margin1').val());
	var BuyCurrency2 = $('#bo-fx-buyCurrency2').val();
	var SellCurrency2 = $('#bo-fx-sellCurrency2').val();
	var Amountbuycurr2 = parseFloat($('#bo-fx-amountBuyCurr2').val());
	var Netrate2 = parseFloat($('#bo-fx-netRate2').val());
	var Margin2 = parseFloat($('#bo-fx-margin2').val());
	var FowardDelivery = $('#bo-fx-fowardDelivery').val();
	var NonDeliverable = ($('#bo-fx-nonDeliverable').val() == 'True') ? true : false;
	var data = [
	{
		"userName": "USER01",
		"party": "TMB Bangkok",
		"counterParty": Counterparty,
		"portfolio": Portfolio,
		"tradeHeader": {
			"tradeDate": Tradedate,
			"tradeDestination": "external"
		},
		"tradeBody": {
			"forexSwap": {
				"forexSwapLeg1": {
					"date": StartDate,
					"sellCurrency": SellCurrency1,
					"buyCurrency": BuyCurrency1,
					"amountBuyCurrency": Amountbuycurr1,
					"netRate": Netrate1,
					"margin": Margin1,
					"forwardDelivery": FowardDelivery
				},
				"forexSwapLeg2": {
					"date": MaturityDate,
					"sellCurrency": SellCurrency2,
					"buyCurrency": BuyCurrency2,
					"amountBuyCurrency": Amountbuycurr2,
					"netRate": Netrate2,
					"margin": Margin2,
					"nonDeliverable": NonDeliverable,
					"fxRateSourceLabel": "BOT01"
				}
			}
		}
	}
	];
	console.log(data);
	return data;
}

