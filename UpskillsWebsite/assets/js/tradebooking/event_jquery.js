$(document).ready(function(){
	$("span.tab").click(function () {
		var formId = $(this).attr("data-tab");
		$('span.tab').removeClass('current');
		$('.tab-content').removeClass('current');
		$(this).addClass('current');
		window.actionDeal = formId;
		$("#" + formId).addClass('current');
		switchTab();
	});

	$(".optionDeal").click(function () {
		window.optionDeal = $(this).attr('id').substring($(this).attr('id').indexOf('option') + 6);
		switchTab();
	});
	
	$('#bo-bond-tradeDate').daterangepicker(picker);
	$('#bo-depo-tradeDate').daterangepicker(picker);
	$('#bo-depo-stream-startperiod').daterangepicker(picker);
	$('#bo-depo-stream-matperiod').daterangepicker(picker);
	$('#bo-irs-maturity').daterangepicker(picker);
	$('#bo-irs-tradeDate').daterangepicker(picker);
	$('#bo-sf-tradeDate').daterangepicker(picker);
	$('#bo-sf-timeToMaturity').daterangepicker(picker);
	$('#bo-fx-tradeDate').daterangepicker(picker);
	$('#bo-fx-startDate').daterangepicker(picker);
	$('#bo-fx-maturityDate').daterangepicker(picker);


	/**
	*	Reset Buttons Extension
	*/

	$("#formBookBond").find(":reset").on("click", function(){
		console.log("DSADSA");
	});

	$("#formBookIRS").find(":reset").on("click", function(){
		$('#bo-irs-maturity').attr("disabled", true);
		$('#bo-irs-maturity').css("background-color", "#e6e6e6");
		console.log("DSADSA");
	});

	$("#formBookDepo").find(":reset").on("click", function(){
		$('#bo-depo-stream-matperiod').attr("disabled", true);
		$('#bo-depo-stream-matperiod').css("background-color", "#e6e6e6");
		console.log("DSADSA");
	});

	$("#formBookSpot").find(":reset").on("click", function(){
		$('#bo-sf-timeToMaturity').attr("disabled", true);
		$('#bo-sf-timeToMaturity').css("background-color", "#e6e6e6");
		console.log("DSADSA");
	});

	$("#formBookFXSwap").find(":reset").on("click", function(){
		$('#bo-fx-maturityDate').attr("disabled", true);
		$('#bo-fx-maturityDate').css("background-color", "#e6e6e6");
		console.log("DSADSA");
	});
	



	/**
	*	Number Input Caculating
	*/
	// BOND
	
	// IRS
	$("#bo-irs-fixedleg-initCapital").on("keyup paste", function() {
	    	$("#bo-irs-floatingleg-initCapital").val($(this).val());
	});	

	// LOAN

	// SF
	var text = $("#bo-sf-contract").val();

	$("#bo-sf-buyCurrency").on("keyup paste", function() {
    	$("#bo-sf-contract").val($(this).val()+"/");
    	text = $("#bo-sf-contract").val();
    	$("#bo-sf-sellCurrency").val("");
	});

	$("#bo-sf-sellCurrency").on("keyup paste", function() {
		$("#bo-sf-contract").val(text+$(this).val());
	});

	$("#bo-sf-amountBuyCurr").on("keyup paste", function() {
		calculatesfAmountBuyCurr();
	});

	$("#bo-sf-fxSpotMargin").on("keyup paste", function() {
		calculateMargin();
		calculateNetRate();
		calculatesfAmountBuyCurr();
	});

	$("#bo-sf-fxSwapMargin").on("keyup paste", function() {
		calculateMargin();
		calculateNetRate();
		calculatesfAmountBuyCurr();
	});

	$("#bo-sf-fxSwapPoint").on("keyup paste", function() {
		// calculateMargin();
		calculateNetRate();
		calculatesfAmountBuyCurr();
	});

	$("#bo-sf-fxSpot").on("keyup paste", function() {
		// calculateMargin();
		calculateNetRate();
		calculatesfAmountBuyCurr();
	});

	$("#optionB2B").change(function(e) {
	    if(this.checked) {
	        $("#B2Brow1").fadeIn(1000);
	        $("#bo-sf-b2bportfolio").attr("required",true);
	        window.optionDeal = 'B2B'

	    }else{
	    	$("#B2Brow1").fadeOut(1000);
	    	$("#bo-sf-b2bportfolio").attr("required",false);
	    	$("#bo-sf-b2bportfolio").val("");
	    	window.optionDeal = 'Spot';
	    }
	});

	// FOREX











	/**
	*	Date Input Manipulation
	*/
	// BOND

	


	
	


	// IRS	
	$("#bo-irs-tradeDate").on('hide.daterangepicker',function(ev,picker){
		activeView1AfterView2Set($("#bo-irs-maturity"),$("#bo-irs-tradeDate"));
	});

	$("#bo-irs-maturity").on('hide.daterangepicker',function (e, picker) {
		var ok = checkDate1LargerDate2WithNotice($("#bo-irs-maturity"),$("#bo-irs-tradeDate"));
		if (!ok) {
			$(e.target).val("");
		};
	});

	// LOAN
	$("#bo-depo-stream-startperiod").on('hide.daterangepicker', function (e, picker) {
		activeView1AfterView2Set($("#bo-depo-stream-matperiod"),$("#bo-depo-stream-startperiod"));
	});

	$("#bo-depo-stream-matperiod").on('hide.daterangepicker',function (e, picker) {
		var ok = checkDate1LargerDate2WithNotice($("#bo-depo-stream-matperiod"),$("#bo-depo-stream-startperiod"));
		if (!ok) {
			$(e.target).val("");
		};
	});


	// TAB CLOSE

	$("#bo-sf-tradeDate").keydown(function(e) {
	   	var code = e.keyCode || e.which;
	    if (code === 9) {  
	        $("#bo-sf-tradeDate").data('daterangepicker').hide();
	    }
	});

	$("#bo-sf-timeToMaturity").keydown(function(e) {
	    var code = e.keyCode || e.which;
	    if (code === 9) {  
	        $("#bo-sf-timeToMaturity").data('daterangepicker').hide();
	    }
	});

	$("#bo-bond-tradeDate").keydown(function(e) {
	    var code = e.keyCode || e.which;
	    if (code === 9) {  
	        $("#bo-bond-tradeDate").data('daterangepicker').hide();
	    }
	});

	$("#bo-irs-tradeDate").keydown(function(e) {
	    var code = e.keyCode || e.which;
	    if (code === 9) {  
	        $("#bo-irs-tradeDate").data('daterangepicker').hide();
	    }
	});

	$("#bo-irs-maturity").keydown(function(e) {
	    var code = e.keyCode || e.which;
	    if (code === 9) {  
	        $("#bo-irs-maturity").data('daterangepicker').hide();
	    }
	});

	$("#bo-depo-tradeDate").keydown(function(e) {
	    var code = e.keyCode || e.which;
	    if (code === 9) {  
	        $("#bo-depo-tradeDate").data('daterangepicker').hide();
	    }
	});

	$("#bo-depo-stream-startperiod").keydown(function(e) {
	    var code = e.keyCode || e.which;
	    if (code === 9) {  
	        $("#bo-depo-stream-startperiod").data('daterangepicker').hide();
	    }
	});

	$("#bo-depo-stream-matperiod").keydown(function(e) {
	    var code = e.keyCode || e.which;
	    if (code === 9) {  
	        $("#bo-depo-stream-matperiod").data('daterangepicker').hide();
	    }
	});

	$("#bo-fx-tradeDate").keydown(function(e) {
	    var code = e.keyCode || e.which;
	    if (code === 9) {  
	        $("#bo-fx-tradeDate").data('daterangepicker').hide();
	    }
	});

	$("#bo-fx-startDate").keydown(function(e) {
	    var code = e.keyCode || e.which;
	    if (code === 9) {  
	        $("#bo-fx-startDate").data('daterangepicker').hide();
	    }
	});

	$("#bo-fx-maturityDate").keydown(function(e) {
	    var code = e.keyCode || e.which;
	    if (code === 9) {  
	        $("#bo-fx-maturityDate").data('daterangepicker').hide();
	    }
	});


	$("#bo-sf-tradeDate").on('hide.daterangepicker',function(ev,picker){
		activeView1AfterView2Set($("#bo-sf-timeToMaturity"),$("#bo-sf-tradeDate"));
		if ($("#bo-sf-timeToMaturity").val() != "")
		{
			getDealType();
			calculateMargin();
			calculateNetRate();
			calculatesfAmountBuyCurr();
		}
	});

	$("#bo-sf-timeToMaturity").on('hide.daterangepicker',function(ev,picker){
		getDealType();
		calculateMargin();
		calculateNetRate();
		calculatesfAmountBuyCurr();
	});


	// FOREX
	$("#bo-fx-tradeDate").on('hide.daterangepicker', function (e, picker) {
		activeView1AfterView2Set($("#bo-fx-maturityDate"),$("#bo-fx-tradeDate"));
	});

	$("#bo-fx-maturityDate").on('hide.daterangepicker',function (e, picker) {
		var ok = checkDate1LargerDate2WithNotice($("#bo-fx-maturityDate"),$("#bo-fx-tradeDate"));
		if (!ok) {
			$(e.target).val("");
		};
	});


	$(document).keypress(function(e) {
    if(e.which == 13) {
        // alert('You pressed enter!');
        e.preventDefault();
    }
});

});